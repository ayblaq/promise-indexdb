# About Async IndexedDB

This is a simple asynchronous IndexedDB library with promises. The library allows for 
table creation, adding data, deleting data and updating dat in indexedDb. 

### Properties

- selectFrom (completed)
- insertInto (completed)
- dropTable  (completed)
- deleteWhere (completed)
- updateWhere 


### .selectFrom(tableName, keys)
Gets data from a specific table where primary key is in keys.  

### .insertInto(tableName, data)
Inserts data into a specific table. Data is a list of object. 

### .dropTable(tableName)
Drops a specific table in the database. 

### .deleteWhere(tableName, pKey)
Deletes a data from a given table name based on primary key.




