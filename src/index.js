class indexedDbConfig {
    get setup() {

        return {
            debug: true,
            version: 1,
            db_name: 'DefaultDatabase',
            //structure is an object of tables, with the key taken as a tablename.
            structure: {
                "user": [
                    { 'name': 'email', 'autoincrement': false, 'primary': true },
                    { 'name': 'firstname', 'unique': false, 'autoincrement': false, 'multientry': false, 'primary': false }
                ]
            }
        };
    }
}

//main indexed db class
let indexedDbModel = function ($config = {}) {
    let $data = $config.setup;
    let version = $data.version ? $data.version : 1,
        db_name = $data.db_name ? $data.db_name : 'myDatabase',
        structure = $data.structure

    this.initDb = new Promise(function (resolve, reject) {
        if (!('indexedDB' in window) || !window) {

            return reject("Error: Browser does not support indexedDb!")
        }
        if ($data instanceof Array) {

            return reject(`Error: Setup ${$data} has to be an Object!.`)
        }
        let request = window.indexedDB.open(db_name, version)
        request.onerror = function (evt) {

            return reject('Error: Unknown', evt.target);
        }
        request.onblocked = function (evt) {
            evt.target.result.close()

            return reject('Error: Other tabs needs to be closed. ')
        }
        request.onsuccess = function () {
            let connection = request.result;
            connection.onversionchange = function () {
                connection.close()
                connection.info('Info: Connection closed... ')
            }

            return resolve(request.result);
        }
        request.onupgradeneeded = function (evt) {
            let db = evt.target.result;
            for (let tableName in structure) {
                if (db.objectStoreNames.contains(tableName) && event.oldVersion < version) {
                    db.deleteObjectStore(tableName)
                }
                let store_struct = structure[tableName]
                store_struct.forEach(element => {
                    if (element.primary == true) {
                        DataStore = db.createObjectStore(tableName, { keyPath: element.name, autoIncrement: element.autoincrement })
                    } else {
                        DataStore.createIndex(element.name, element.name, { unique: element.unique, multiEntry: element.multientry })
                    }
                })
            }

            // return resolve(request.result)
        }
    })
}

/**
 * Insert data into a given table  
 *
 * */
indexedDbModel.prototype.insertInto = function (tableName, data) {

    return new Promise((resolve, reject) => {
        this.initDb.then(function (db) {
            try {
                let trans = db.transaction(tableName, 'readwrite');
                trans.oncomplete = function (event) {
                    return resolve("Success: Data stored succesfully")
                }
                trans.onerror = function (event) {
                    return reject("Error: " + event.target)
                }
                let store = trans.objectStore(tableName)
                for (var key in data) {
                    var req = store.put(data[key]);
                    req.onerror = function (evt) {
                        return reject("Error: " + evt.target)
                    }
                }
            } catch (e) {
                return reject(e)
            }
        })
    })
}

/**
 * gets data from given table based on given list of keys
 * 
 * */
indexedDbModel.prototype.selectFrom = function (tableName, opts) {
    let data = {};
    return new Promise((resolve, reject) => {
        this.initDb.then(function (db) {

            let data = {};
            let trans = db.transaction([tableName])
            trans.oncomplete = function (evt) {
                return resolve(data)
            }
            trans.onerror = function (evt) {
                return reject("Unable to retrieve data from Model")
            }
            let store = trans.objectStore(tableName)
            for (var key in opts) {
                let request = store.get(opts[key])
                let id = opts[key]
                request.onsuccess = function (evt) {
                    if (evt.target.result !== undefined) {
                        data[id] = evt.target.result
                    }
                }
                request.onerror = function (evt) {
                    return reject("Error: Unable to retrieve data from Mode")
                }
            }
        });
    })
}

/**
 * Updates a given table column based on a key
 *
 * */
indexedDbModel.prototype.updateWhere = function (tableName, key, value) {
    return new Promise((resolve, reject) => {
        this.initDb.then(db => {
            this.selectFrom(tableName, key).then(data => {
                var trans = db.transaction(tableName, "readwrite");
                var store = trans.objectStore(tableName);
                value = Object.assign(data, value);
                let request = store.put(value);
                request.onsuccess = function () {
                    return resolve(true);
                };
                request.onerror = function () {
                    console.warn("Error: Could not update !")
                    return reject(true);
                };
            });
        });
    });
}

/**
 * Drops a given table. 
 *
 * */
indexedDbModel.prototype.dropTable = function (tableName) {
    return new Promise((resolve, reject) => {
        self.initDb.then(function (db) {
            try {
                let trans = db.transaction(store_name, 'readwrite')
                let store = trans.objectStore(tableName);
                let request = store.clear()
                request.onsuccess = function (evt) {
                    resolve(true)
                }
                request.onerror = function (evt) {
                    console.warn("Error: Could not drop table !")
                    reject(true)
                }
                request.onblocked = function (evt) {
                    console.warn("Error: Could not drop table !")
                    reject(true)
                }
            } catch (e) {
                console.warn(`${e}`)
                reject(true)
            }
        })
    })
}

module.exports = {
    indexedDbConfig,
    indexedDbModel
}